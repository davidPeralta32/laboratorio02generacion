package com.example.laboratorio02generacion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {
           val etNacimiento:Int = etNacimiento.text.toString().toInt()
            when(etNacimiento){
                in 1994 .. 2010 -> {
                    tvGeneracion.text = "Generacion Z"
                    imgv_generacio.setImageResource(R.drawable.z_generacion)
                }
                in 1981 .. 1993 -> {
                    tvGeneracion.text ="Generacion Y"
                    imgv_generacio.setImageResource(R.drawable.y_generacion)
                }
                in 1969 .. 1980 -> {
                    tvGeneracion.text ="Generacion X"
                    imgv_generacio.setImageResource(R.drawable.x_generacion)
                }
                in 1949 .. 1968 -> {
                    tvGeneracion.text ="Baby Boom"
                    imgv_generacio.setImageResource(R.drawable.baby_generacion)
                }
                in 1930 .. 1948 ->  {
                    tvGeneracion.text = "Silent Generation"
                    imgv_generacio.setImageResource(R.drawable.silen_generacion)
                }
                else -> {
                    tvGeneracion.text = "No hay una generacion para esta fecha"
                }
            }

        }
    }
}